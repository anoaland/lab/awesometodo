import React, { Component } from 'react'
import { Text, View } from 'react-native'
import { AppData } from './data'
import { AppStore } from './store'
import { MainScreen } from './views/screens/main'
import { AppStyle } from './views/styles'

interface State {
  ready: boolean
  error?: string
}

/**
 * Application root component.
 */
export default class App extends Component<any, State> {
  constructor(props: any) {
    super(props)
    this.state = {
      ready: false
    }

    this.prepare = this.prepare.bind(this)
  }

  async componentDidMount() {
    try {
      await this.prepare()
      this.setState({ ready: true })
    } catch (error) {
      this.setState({ ready: true, error })
    }
  }

  render() {
    const { ready, error } = this.state

    if (!ready) {
      return null
    }

    if (error) {
      return this.renderError(error)
    }

    return this.renderMain()
  }

  /**
   * Render main view of application.
   * Do not rename this function -- anoa will look this function
   * to wrap your main view with neccessary provider as if needed.
   */
  renderMain() {
    return (
      <AppStyle.Provider
        getDefault={async () => {
          return (await AppData.Settings.getTheme()) as any
        }}
        onChange={async theme => {
          await AppData.Settings.setTheme(theme)
        }}
      >
        <AppStore.Provider>
          <MainScreen />
        </AppStore.Provider>
      </AppStyle.Provider>
    )
  }

  /**
   * Show this view when preparation were failed.
   * @param error error message
   */
  renderError(error: string) {
    // TODO: do nicer error handler
    return (
      <View
        style={{
          flex: 1,
          alignContent: 'center',
          alignItems: 'center',
          backgroundColor: 'red',
          padding: 60
        }}
      >
        <Text>Oops!</Text>
        <Text style={{ color: 'white' }}>{error}</Text>
      </View>
    )
  }

  /**
   * Prepare application before showing main view.
   * Do not rename this function -- anoa will look this function
   * to add necessary calls as if needed.
   */
  async prepare(): Promise<void> {
    await AppData.init()
    await AppStore.init(async () => {
      return {
        todo: {
          tasks: await AppData.Todo.getTasks(false),
          completedTasks: await AppData.Todo.getTasks(true)
        }
      }
    })

    // TODO: Load anything before main screen shown
  }
}

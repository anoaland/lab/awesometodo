import React from 'react'
import { Text, TouchableOpacity, View } from 'react-native'
import { AppStyle } from '../../styles'
import { ButtonProps } from './props'

export const Button = AppStyle.withTheme<ButtonProps>(({ text, onPress }) => {
  return (
    <TouchableOpacity onPress={onPress}>
      <View style={styles.container}>
        <Text style={styles.text}>{text}</Text>
      </View>
    </TouchableOpacity>
  )
})

const styles = AppStyle.createStyleSheet(({ colors }) => ({
  container: {
    backgroundColor: colors.dark,
    height: 40,
    flexDirection: 'row' as 'row',
    justifyContent: 'center' as 'center',
    alignItems: 'center' as 'center'
  },
  text: {
    color: colors.light
  }
}))

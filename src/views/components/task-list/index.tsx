import React from 'react'
import { Text, TouchableOpacity, View } from 'react-native'
import { AppStore } from '../../../store'
import { completeTaskAction } from '../../../store/actions/add-task-action'
import { AppStyle } from '../../styles'
import { TaskListActionProps, TaskListProps, TaskListStateProps } from './props'

@AppStyle.withThemeClass()
@AppStore.withStoreClass<TaskListStateProps, TaskListActionProps>(
  state => ({ todoTasks: state.todo.tasks }),
  dispatch => ({ completeTask: task => dispatch(completeTaskAction(task)) })
)
export class TaskList extends React.Component<TaskListProps> {
  constructor(props: TaskListProps) {
    super(props)
  }

  public render() {
    const {
      todoTasks,
      completeTask,
      theme: { styles }
    } = this.props as Required<TaskListProps>

    return (
      <View>
        {todoTasks.map((task, i) => (
          <TouchableOpacity
            key={`task-${i}`}
            onPress={() => {
              completeTask(task)
            }}
          >
            <Text style={styles.taskItem}>{task.name}</Text>
          </TouchableOpacity>
        ))}
      </View>
    )
  }
}

import { Task } from '../../../data/models'
import { AppStyleProps } from '../../styles'

export interface TaskListProps
  extends Partial<AppStyleProps>,
    Partial<TaskListStateProps>,
    Partial<TaskListActionProps> {}

export interface TaskListStateProps {
  todoTasks: Task[]
}

export interface TaskListActionProps {
  completeTask: (task: Task) => void
}

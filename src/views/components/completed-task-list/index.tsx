import React from 'react'
import { Text, TouchableOpacity, View } from 'react-native'
import { AppStore } from '../../../store'
import { incompleteTaskAction } from '../../../store/actions/add-task-action'
import { AppStyle } from '../../styles'
import {
  CompletedTaskListActionProps,
  CompletedTaskListProps,
  CompletedTaskListStateProps
} from './props'

@AppStyle.withThemeClass()
@AppStore.withStoreClass<
  CompletedTaskListStateProps,
  CompletedTaskListActionProps
>(
  state => ({ todoCompletedTasks: state.todo.completedTasks }),
  dispatch => ({ incompleteTask: task => dispatch(incompleteTaskAction(task)) })
)
export class CompletedTaskList extends React.Component<CompletedTaskListProps> {
  constructor(props: CompletedTaskListProps) {
    super(props)
  }

  public render() {
    const {
      todoCompletedTasks,
      incompleteTask,
      theme: { styles }
    } = this.props as Required<CompletedTaskListProps>
    return (
      <View>
        {todoCompletedTasks.map((task, i) => (
          <TouchableOpacity
            key={`completed-task-${i}`}
            onPress={() => {
              incompleteTask(task)
            }}
          >
            <Text style={styles.taskItem}>{task.name}</Text>
          </TouchableOpacity>
        ))}
      </View>
    )
  }
}

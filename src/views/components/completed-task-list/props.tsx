import { Task } from '../../../data/models'
import { AppStyleProps } from '../../styles'

export interface CompletedTaskListProps
  extends Partial<AppStyleProps>,
    Partial<CompletedTaskListStateProps>,
    Partial<CompletedTaskListActionProps> {}

export interface CompletedTaskListStateProps {
  todoCompletedTasks: Task[]
}

export interface CompletedTaskListActionProps {
  incompleteTask: (task: Task) => void
}

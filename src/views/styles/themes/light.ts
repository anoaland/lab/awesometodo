import { createTheme } from 'anoa-react-native-theme'

export const LightTheme = createTheme(
  {
    name: 'Light',
    colors: {
      primary: '#58A4B0',
      secondary: '#373F51',
      light: '#D8DBE2',
      dark: '#005779'
    },
    spaces: {
      small: 10,
      medium: 20,
      big: 40
    }
  },
  props => ({
    container: {
      flex: 1
    },
    screenContainer: {
      flex: 1,
      backgroundColor: props.colors.primary,
      padding: props.spaces.medium
    },
    header: {
      fontSize: 20,
      textAlign: 'center',
      margin: 10
    },
    taskItem: {
      padding: props.spaces.small,
      backgroundColor: props.colors.light,
      color: props.colors.dark,
      borderRadius: props.spaces.small,
      marginBottom: props.spaces.small
    }
  })
)

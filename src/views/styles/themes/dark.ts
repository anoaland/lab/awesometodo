import { LightTheme } from './light'

export const DarkTheme = LightTheme.extend(
  {
    name: 'Dark',
    colors: {
      primary: '#474973',
      secondary: '#8D9AAA',
      dark: '#0F1330'
    }
  },
  props => ({
    // override default theme styles
  })
)

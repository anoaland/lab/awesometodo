import React from 'react'
import { View } from 'react-native'
import { CompletedTaskList } from '../../components/completed-task-list'
import { AppStyle } from '../../styles'
import { CompletedTasksScreenProps } from './props'

@AppStyle.withThemeClass()
export class CompletedTasksScreen extends React.Component<
  CompletedTasksScreenProps
> {
  constructor(props: CompletedTasksScreenProps) {
    super(props)
  }

  public render() {
    const {
      theme: { styles }
    } = this.props as Required<CompletedTasksScreenProps>
    return (
      <View style={styles.screenContainer}>
        <CompletedTaskList />
      </View>
    )
  }
}

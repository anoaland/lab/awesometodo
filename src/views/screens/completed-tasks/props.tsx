import { AppStyleProps } from '../../styles'

export interface CompletedTasksScreenProps extends Partial<AppStyleProps> {}

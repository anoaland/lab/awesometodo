import React from 'react'
import { Text, View } from 'react-native'
import { AppStyle } from '../../styles'
import { MainScreenNav } from './nav'
import { MainScreenProps } from './props'

@AppStyle.withThemeClass()
export class MainScreen extends React.Component<MainScreenProps> {
  constructor(props: MainScreenProps) {
    super(props)
  }

  public render() {
    const {
      theme: { styles }
    } = this.props as Required<MainScreenProps>
    return (
      <View style={styles.container}>
        <Text style={styles.header}>Awesome Todo App</Text>
        <MainScreenNav />
      </View>
    )
  }
}

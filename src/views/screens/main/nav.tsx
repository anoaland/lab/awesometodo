import React from 'react'
import { createMaterialTopTabNavigator } from 'react-navigation'
import { AppStyle } from '../../styles'
import { CompletedTasksScreen } from '../completed-tasks'
import { SettingsScreen } from '../settings'
import { TodoListScreen } from '../todo-list'

let initialRouteName = 'TodoList'

export const MainScreenNav = AppStyle.withTheme(() => {
  const _MainScreenNav = createMaterialTopTabNavigator(
    {
      TodoList: {
        navigationOptions: {
          title: 'Todo List'
        },
        screen: TodoListScreen
      },
      CompletedTasks: {
        navigationOptions: {
          title: 'Completed Tasks'
        },
        screen: CompletedTasksScreen
      },
      Settings: SettingsScreen
    },
    {
      tabBarOptions: {
        showIcon: false,
        tabStyle: styles.tab
      },
      tabBarPosition: 'bottom',
      initialRouteName
    }
  )

  return (
    <_MainScreenNav
      onNavigationStateChange={(_prev, next) => {
        initialRouteName = next.routes[next.index].routeName
      }}
    />
  )
})

const styles = AppStyle.createStyleSheet(({ colors }) => ({
  tab: {
    backgroundColor: colors.dark
  }
}))

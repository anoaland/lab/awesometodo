import { AppStyleProps } from '../../styles'

export interface TodoListScreenProps
  extends Partial<AppStyleProps>,
    Partial<TodoListScreenActionProps> {}

export interface TodoListScreenActionProps {
  addTask: (task: string) => void
}

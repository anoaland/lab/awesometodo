import React from 'react'
import { TextInput, View } from 'react-native'
import { AppStore } from '../../../store'
import { addTaskAction } from '../../../store/actions/add-task-action'
import { Button } from '../../components/button'
import { TaskList } from '../../components/task-list'
import { AppStyle } from '../../styles'
import { TodoListScreenActionProps, TodoListScreenProps } from './props'
import { TodoListScreenState } from './state'

@AppStyle.withThemeClass()
@AppStore.withStoreClass<any, TodoListScreenActionProps>(null, dispatch => ({
  addTask: task => dispatch(addTaskAction(task))
}))
export class TodoListScreen extends React.Component<
  TodoListScreenProps,
  TodoListScreenState
> {
  constructor(props: TodoListScreenProps) {
    super(props)
    this.state = {
      taskName: ''
    }
  }

  public render() {
    const {
      theme: { styles },
      addTask
    } = this.props as Required<TodoListScreenProps>
    return (
      <View style={styles.screenContainer}>
        <TextInput
          style={todoStyles.textInput}
          value={this.state.taskName}
          onChangeText={taskName => this.setState({ taskName })}
        />
        <Button
          text="Add"
          onPress={() => {
            addTask(this.state.taskName)
            this.setState({ taskName: '' })
          }}
        />
        <View style={todoStyles.divider} />
        <TaskList />
      </View>
    )
  }
}

const todoStyles = AppStyle.createStyleSheet(({ colors, spaces }) => ({
  textInput: {
    backgroundColor: colors.secondary,
    paddingHorizontal: spaces.small,
    color: colors.light
  },
  divider: {
    height: spaces.small
  }
}))

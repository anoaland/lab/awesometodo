import React from 'react'
import { View } from 'react-native'
import { Button } from '../../components/button'
import { AppStyle } from '../../styles'
import { SettingsScreenProps } from './props'

@AppStyle.withThemeClass()
export class SettingsScreen extends React.Component<SettingsScreenProps> {
  constructor(props: SettingsScreenProps) {
    super(props)
  }

  public render() {
    const {
      theme: { change, props, styles }
    } = this.props as Required<SettingsScreenProps>
    return (
      <View style={styles.screenContainer}>
        <Button
          text={`Switch Theme. Current Theme is: ${props.name}`}
          onPress={() => {
            if (props.name === 'Light') {
              change('dark')
            } else {
              change('default')
            }
          }}
        />
      </View>
    )
  }
}

import { Db } from 'sqlite-ts'
import { Models } from '..'

export class Settings {
  db: Db<Models>
  constructor(db: Db<Models>) {
    this.db = db
  }

  async getTheme(): Promise<string> {
    const data = await this.db.tables.Settings.single()
    if (!data) {
      return 'default'
    }

    return data.theme
  }

  async setTheme(theme: string) {
    await this.db.tables.Settings.upsert({ id: 1, theme })
  }
}

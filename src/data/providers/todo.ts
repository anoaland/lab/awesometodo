import { Db } from 'sqlite-ts'
import { Models } from '..'
import { Task } from '../models'

export class Todo {
  db: Db<Models>
  constructor(db: Db<Models>) {
    this.db = db
  }

  async addTask(name: string): Promise<Task> {
    const res = await this.db.tables.Task.insert({ name, done: false })
    return {
      id: res.insertId,
      name,
      done: false
    }
  }

  async changeTaskStatus(id: number, done: boolean) {
    await this.db.tables.Task.update({ done }).where(c => c.equals({ id }))
  }

  async getTasks(done: boolean): Promise<Task[]> {
    return await this.db.tables.Task.select().where(c => c.equals({ done }))
  }
}

import { Column, Primary } from 'sqlite-ts'

export class Task {
  @Primary()
  id?: number = 0

  @Column('NVARCHAR')
  name: string = ''

  @Column('BOOLEAN')
  done: boolean = false
}

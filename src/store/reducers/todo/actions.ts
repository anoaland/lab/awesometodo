import { Task } from '../../../data/models'

type TodoActions =
  | {
      type: 'TODO/TASKS'
      payload: Task[]
    }
  | {
      type: 'TODO/COMPLETED_TASKS'
      payload: Task[]
    }

export default TodoActions

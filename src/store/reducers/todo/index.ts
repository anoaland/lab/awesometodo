import { Reducer } from 'redux'
import TodoActions from './actions'
import TodoState from './state'

const TodoReducer: Reducer<TodoState, TodoActions> = (
  state = {
    tasks: [],
    completedTasks: []
  },
  action
) => {
  switch (action.type) {
    case 'TODO/TASKS':
      return { ...state, tasks: action.payload }

    case 'TODO/COMPLETED_TASKS':
      return { ...state, completedTasks: action.payload }

    default:
      return state
  }
}

export default TodoReducer

import { Task } from '../../../data/models'

export default interface TodoState {
  tasks: Task[]
  completedTasks: Task[]
}

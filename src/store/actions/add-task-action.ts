import { AppThunkAction } from '..'
import { AppData } from '../../data'
import { Task } from '../../data/models'

export function addTaskAction(task: string): AppThunkAction {
  return async (dispatch, getState) => {
    const {
      todo: { tasks }
    } = getState()

    const newTask = await AppData.Todo.addTask(task)

    dispatch({ type: 'TODO/TASKS', payload: [...tasks, newTask] })
  }
}

export function completeTaskAction(task: Task): AppThunkAction {
  return async (dispatch, getState) => {
    const {
      todo: { tasks, completedTasks }
    } = getState()

    await AppData.Todo.changeTaskStatus(task.id as number, true)

    dispatch({
      type: 'TODO/TASKS',
      payload: [...tasks.filter(t => t !== task)]
    })

    dispatch({
      type: 'TODO/COMPLETED_TASKS',
      payload: [...completedTasks, task]
    })
  }
}

export function incompleteTaskAction(task: Task): AppThunkAction {
  return async (dispatch, getState) => {
    const {
      todo: { tasks, completedTasks }
    } = getState()

    await AppData.Todo.changeTaskStatus(task.id as number, false)

    dispatch({
      type: 'TODO/COMPLETED_TASKS',
      payload: [...completedTasks.filter(t => t !== task)]
    })

    dispatch({
      type: 'TODO/TASKS',
      payload: [...tasks, task]
    })
  }
}
